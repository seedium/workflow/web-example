import type { RegistrationFormValuesType } from 'modules/auth/types/auth0-register.types';

export const REGISTER_INITIAL_VALUES: RegistrationFormValuesType = {
  email: '',
  username: '',
  password: '',
  isAcceptedTerms: false,
};
