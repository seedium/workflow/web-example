import * as yup from 'yup';

// ** Types
import type { ForgotPasswordType } from 'modules/auth/types/forgot-password.types';

export const forgotPasswordSchema: yup.SchemaOf<ForgotPasswordType> = yup
  .object()
  .shape({
    email: yup
      .string()
      .email('Invalid email')
      .required('Field cannot be empty'),
  });
