import * as yup from 'yup';

// ** Types
import { ResetPasswordType } from '../types/reset-password.types';

// ** Constants
import { MIN_PASSWORD } from 'modules/auth/constants/password.constant';

export const resetPasswordSchema: yup.SchemaOf<ResetPasswordType> = yup
  .object()
  .shape({
    newPassword: yup
      .string()
      .required('Field cannot be empty')
      .min(MIN_PASSWORD, `Required more than ${MIN_PASSWORD} symbols`),
    confirmPassword: yup
      .string()
      .required('Field cannot be empty')
      .min(MIN_PASSWORD, `Required more than ${MIN_PASSWORD} symbols`)
      .test('newPassword', 'Passwords do not match', function (value) {
        return this.parent.newPassword === value;
      }),
  });
