import { WebAuth, LogoutOptions, Auth0ParseHashError } from 'auth0-js';
import { addSeconds } from 'date-fns';

// ** Utils
import {
  addToLocalStorage,
  getFromLocalStorage,
  removeFromLocalStorage,
} from 'modules/core/utils/storage.utils';

// ** Types
import type { Auth0RegisterDataType } from 'modules/auth/types/auth0-register.types';
import type { LoginCredentialsType } from 'modules/auth/types/login.types';
import type { ForgotPasswordType } from 'modules/auth/types/forgot-password.types';
import {
  isAuth0HashData,
  isCheckSessionResult,
} from 'modules/auth/types/auth0-service.types';

// ** Constants
import { authRoutes } from 'modules/core/constants/routes/auth-routes.constants';

class Auth0Service {
  private readonly webAuth = new WebAuth({
    domain: process.env.REACT_APP_AUTH0_DOMAIN,
    clientID: process.env.REACT_APP_AUTH0_CLIENT_ID,
    audience: process.env.REACT_APP_AUDIENCE,
    redirectUri: `${window.location.origin}${authRoutes.callback}`,
    responseType: 'token',
  });

  public async signIn(credentials: LoginCredentialsType) {
    return new Promise((resolve, reject) => {
      this.webAuth.login(credentials, (err, result) => {
        if (err) {
          reject(err);
        }

        if (result) {
          resolve(result);
        }
      });
    });
  }

  public async signUp(options: Auth0RegisterDataType) {
    return new Promise((resolve, reject) => {
      this.webAuth.signup(
        { ...options, connection: process.env.REACT_APP_AUTH0_CONNECTION },
        (err, result) => {
          if (result) {
            addToLocalStorage('isRememberAuthorization', true);
            this.signIn({ password: options.password, email: options.email });
            resolve(result);
          }

          if (err) {
            reject(err);
          }
        }
      );
    });
  }

  public parseUrlHash = async () => {
    const isRememberAuthorization = getFromLocalStorage(
      'isRememberAuthorization'
    );

    return new Promise((resolve, reject) => {
      this.webAuth.parseHash((err, result) => {
        if (!err && !result) {
          throw new Error('Invalid url hash');
        }

        if (err) {
          reject(err);
        }

        if (isAuth0HashData(result)) {
          if (!isRememberAuthorization) {
            this.setAuthorizationExpirationTimeToken(result.expiresIn);
          }

          resolve(result);
        }
      });
    });
  };

  private setAuthorizationExpirationTimeToken = (expiresIn: number): void => {
    const expiresAt: number = +addSeconds(Date.now(), expiresIn);

    addToLocalStorage('expiresAt', expiresAt);
  };

  public async checkSession() {
    return new Promise((resolve, reject) => {
      this.webAuth.checkSession(
        {},
        (err: Auth0ParseHashError | null, result) => {
          if (err) {
            reject(err);
          }

          if (result && isCheckSessionResult(result)) {
            resolve(result);
          }
        }
      );
    });
  }

  public changePassword = (data: ForgotPasswordType): Promise<string> => {
    return new Promise((resolve, reject) => {
      this.webAuth.changePassword(
        {
          email: data.email,
          connection: process.env.REACT_APP_AUTH0_CONNECTION,
        },
        (err, result) => {
          if (err) {
            reject(err);
          }

          if (result) {
            resolve(result);
          }
        }
      );
    });
  };

  public async logout(options: LogoutOptions) {
    removeFromLocalStorage('expiresAt');
    await this.webAuth.logout(options);
  }
}

export const auth0Service = new Auth0Service();
