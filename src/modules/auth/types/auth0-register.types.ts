export interface Auth0RegisterDataType {
  given_name: string;
  family_name: string;
  email: string;
  password: string;
}

export interface UserRegisterType {
  email: string;
  password: string;
  username: string;
}

export interface RegistrationFormValuesType extends UserRegisterType {
  isAcceptedTerms: boolean;
}
