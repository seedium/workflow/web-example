export type ResetPasswordType = {
  newPassword: string;
  confirmPassword: string;
};
