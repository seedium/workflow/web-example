// ** React Imports
import React, { useState, useCallback } from 'react';

// ** Third Party Components
import classnames from 'classnames';
import { ChevronUp } from 'react-feather';
import { Collapse, Card, CardHeader, CardBody, CardTitle } from 'reactstrap';

// ** Types
import { AccordionProps } from './accordion.interfaces';

import styles from './accordion.module.scss';

export const Accordion: React.FC<AccordionProps> = ({
  data,
  rootClassName = '',
  isSingleOpening,
  type,
  active,
}) => {
  const defaultActive = (): number[] => {
    if (isSingleOpening) {
      return [active[0]];
    }

    return [...active];
  };

  const [openCollapse, setOpenCollapse] = useState<number[]>(defaultActive());

  const handleCollapseToggle = useCallback(
    (id: number) => {
      if (isSingleOpening) {
        if (id === openCollapse[0]) {
          setOpenCollapse([]);
        } else {
          setOpenCollapse([id]);
        }
      } else {
        const arr = openCollapse;
        const index = arr.indexOf(id);

        if (arr.includes(id)) {
          arr.splice(index, 1);
          setOpenCollapse([...arr]);
        } else {
          arr.push(id);
          setOpenCollapse([...arr]);
        }
      }
    },
    [isSingleOpening, openCollapse]
  );

  return (
    <div
      className={classnames('collapse-icon', rootClassName, {
        'collapse-default': !type,
        'collapse-shadow': type === 'shadow',
        'collapse-border': type === 'border',
        'collapse-margin': type === 'margin',
      })}
    >
      {data.map(({ title, content, className }, index) => {
        const isOpen: boolean = isSingleOpening
          ? openCollapse[0] === index
          : openCollapse.includes(index);

        return (
          <Card
            className={classnames('app-collapse', className, {
              open: isOpen && type === 'shadow',
            })}
            key={index}
          >
            <CardHeader
              className={classnames(
                'align-items-center',
                'defaultSidePadding',
                styles.header,
                {
                  collapsed: !isOpen,
                }
              )}
              onClick={() => handleCollapseToggle(index)}
            >
              <CardTitle
                className={classnames('collapse-title', styles.header)}
              >
                {title}
              </CardTitle>
              <ChevronUp size={18} />
            </CardHeader>
            <Collapse isOpen={isOpen}>
              <CardBody className='defaultSidePadding pt-1'>{content}</CardBody>
            </Collapse>
          </Card>
        );
      })}
    </div>
  );
};
