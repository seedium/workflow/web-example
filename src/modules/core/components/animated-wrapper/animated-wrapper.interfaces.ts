import { Classes } from 'modules/core/types/classes.types';

export type AnimatedWrapperProps = {
  classes?: Classes<'wrapper'>;
};
