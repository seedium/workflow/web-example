import { AvatarProps } from 'modules/core/components/avatar/avatar.interfaces';

export type AvatarGroupProps = {
  className?: string;
  data: AvatarProps[];
};
