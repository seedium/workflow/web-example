// ** React Imports
import React, { Fragment } from 'react';

// ** Third Party Components
import classnames from 'classnames';
import { UncontrolledTooltip } from 'reactstrap';

// ** Custom Components
import { Avatar } from 'modules/core/components';

// ** Types
import { AvatarGroupProps } from './avatar-group.interfaces';

export const AvatarGroup: React.FC<AvatarGroupProps> = ({
  data,
  className,
}) => {
  return (
    <div className={classnames('avatar-group', className)}>
      {data.map((item, i) => (
        <Fragment key={i}>
          {item.name && (
            <UncontrolledTooltip placement='top' target={item.name}>
              {item.name}
            </UncontrolledTooltip>
          )}
          <Avatar
            {...item}
            tag='div'
            className='pull-up'
            {...(item.name ? { id: item.name } : {})}
          />
        </Fragment>
      ))}
    </div>
  );
};
