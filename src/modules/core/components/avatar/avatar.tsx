// ** React Imports
import React from 'react';

// ** Third Party Components
import { Badge } from 'reactstrap';
import classnames from 'classnames';

// ** Types
import { AvatarProps } from './avatar.interfaces';

// ** Styles
import styles from './avatar.module.scss';

export const Avatar: React.FC<AvatarProps> = (props) => {
  // ** Props
  const {
    color,
    className = '',
    imgClassName = '',
    initials,
    size,
    badgeUp,
    content = '',
    icon,
    badgeColor,
    badgeText,
    img = '',
    imgHeight,
    imgWidth,
    status,
    contentStyles,
    ...rest
  } = props;

  // ** Function to extract initials from content
  const getInitials = (str: string) => {
    const results: string[] = [];
    const wordArray = str.split(' ');

    wordArray.forEach((e) => {
      const str = e[0] as string;

      results.push(str);
    });

    return results.join('');
  };

  return (
    <div
      className={classnames('avatar', {
        [className]: className,
        [`bg-${color}`]: color,
        [`avatar-${size}`]: size,
      })}
      style={{
        width: imgWidth && !size ? imgWidth : '32px',
        height: imgHeight && !size ? imgHeight : '32px',
      }}
      {...rest}
    >
      <div className={styles.inner}>
        {!img ? (
          <span
            className={classnames('avatar-content', {
              'position-relative': badgeUp,
            })}
            style={contentStyles}
          >
            {initials ? getInitials(content) : content}

            {icon ? icon : null}
            {badgeUp ? (
              <Badge
                color={badgeColor ? badgeColor : 'primary'}
                className='badge-sm badge-up'
                pill
              >
                {badgeText ? badgeText : '0'}
              </Badge>
            ) : null}
          </span>
        ) : (
          <img
            className={classnames({
              [imgClassName]: imgClassName,
            })}
            src={img}
            alt='avatarImg'
          />
        )}
      </div>
      {status ? (
        <span
          className={classnames({
            [`avatar-status-${status}`]: status,
            [`avatar-status-${size}`]: size,
          })}
        />
      ) : null}
    </div>
  );
};
