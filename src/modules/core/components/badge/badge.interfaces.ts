import { Color } from 'modules/core/types/theme.types';

export type BadgeProps = {
  color: Color;
  text: string;
  className?: string;
};
