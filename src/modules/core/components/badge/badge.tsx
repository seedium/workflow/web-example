import React from 'react';
import { Badge as DefaultBadge } from 'reactstrap';
import classnames from 'classnames';

// ** Types
import { BadgeProps } from './badge.interfaces';

// ** Styles
import styles from './badge.module.scss';

export const Badge: React.FC<BadgeProps> = ({ color, text, className }) => {
  return (
    <div className='text-capitalize'>
      <DefaultBadge
        className={classnames(styles.badge, className)}
        color={color}
      >
        {text}
      </DefaultBadge>
    </div>
  );
};
