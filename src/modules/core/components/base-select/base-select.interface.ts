import { OnChangeValue } from 'react-select';

export interface IBaseOption {
  value: string;
  label: string;
}

export type BaseSelectProps = {
  id: string;
  options: IBaseOption[];
  defaultValue?: IBaseOption;
  value?: IBaseOption;
  onChange: (newValue: OnChangeValue<IBaseOption, boolean>) => void;
  isClearable?: boolean;
  isCreatable?: boolean;
  isLoading?: boolean;
  isDisabled?: boolean;
  name?: 'loading' | 'clear';
};
