import { FC } from 'react';
import classnames from 'classnames';
import { Tooltip } from 'reactstrap';

// ** Types
import type { ControlledTooltipProps } from './controlled-tooltip.interfaces';

// ** Styles
import styles from '../tooltip/tooltip.module.scss';

export const ControlledTooltip: FC<ControlledTooltipProps> = ({
  className,
  id,
  text,
  isOpen,
}) => {
  return (
    <Tooltip
      target={id}
      placement='top'
      isOpen={isOpen}
      innerClassName={classnames(styles.tooltip, className)}
    >
      {text}
    </Tooltip>
  );
};
