import { ReactNode } from 'react';

export type ClipboardProps = {
  value: string;
  children: ReactNode;
  onCopy?: () => void;
  clipboardText?: string;
  id: string;
};
