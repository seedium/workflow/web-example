import { FC, useEffect, useRef } from 'react';
import { CopyToClipboard } from 'react-copy-to-clipboard';

// ** Components
import { ControlledTooltip } from 'modules/core/components';

// ** Hooks
import { useBoolean } from 'modules/core/hooks/use-boolean';

import type { ClipboardProps } from './copy-to-clipboard.interface';

const HIDE_TIP_TIMEOUT = 3000;

export const Clipboard: FC<ClipboardProps> = ({
  value,
  children,
  onCopy,
  id,
  clipboardText = 'Link copied to clipboard',
}) => {
  const [isShowTooltip, setIsShowTooltip] = useBoolean(false);
  const timeoutRef = useRef<any>(null);

  const copyHandler = () => {
    if (onCopy) {
      onCopy();
    }

    setIsShowTooltip.on();

    timeoutRef.current = setTimeout(() => {
      setIsShowTooltip.off();
    }, HIDE_TIP_TIMEOUT);
  };

  useEffect(() => {
    return () => {
      clearTimeout(timeoutRef.current);
    };
  }, []);

  return (
    <>
      <ControlledTooltip id={id} text={clipboardText} isOpen={isShowTooltip} />
      <CopyToClipboard onCopy={copyHandler} text={value}>
        {children}
      </CopyToClipboard>
    </>
  );
};
