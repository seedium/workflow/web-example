import { FC, useMemo } from 'react';
import classNames from 'classnames';
import {
  DropdownToggle,
  DropdownMenu,
  DropdownItem,
  UncontrolledDropdown,
} from 'reactstrap';

// ** Types
import type { DropdownProps } from './dropdown.interface';

export const Dropdown: FC<DropdownProps> = ({
  dropdownList,
  toggleSettings,
  defaultValue = '',
  defaultIcon: DefaultIcon,
  menuSettings,
}) => {
  const isAvailableDropdown = useMemo(
    () => dropdownList.some((item) => item.isAvailable),
    [dropdownList]
  );

  return isAvailableDropdown ? (
    <UncontrolledDropdown>
      <DropdownToggle {...toggleSettings}>
        {DefaultIcon && <DefaultIcon className='pr-1' size={18} />}
        {defaultValue}
      </DropdownToggle>
      <DropdownMenu {...menuSettings}>
        {dropdownList.map(
          (
            { content, icon: DropdownIcon, settings, onClick, isAvailable },
            i
          ) =>
            isAvailable && (
              <DropdownItem
                key={i}
                className={classNames('w-100', settings?.className)}
                {...settings}
                onClick={() => onClick()}
              >
                {DropdownIcon && (
                  <DropdownIcon
                    className={classNames(settings?.iconClassName)}
                    size={14}
                  />
                )}
                <span className='pl-1'>{content}</span>
              </DropdownItem>
            )
        )}
      </DropdownMenu>
    </UncontrolledDropdown>
  ) : null;
};
