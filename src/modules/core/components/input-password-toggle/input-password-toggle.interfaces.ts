import React from 'react';

export type InputPasswordToggleProps = {
  hideIcon?: React.ReactNode;
  showIcon?: React.ReactNode;
  visible?: boolean;
  className?: string;
  placeholder?: string;
  iconSize?: number;
  inputClassName?: string;
  label?: string;
  htmlFor?: string;
  id?: string;
  invalid?: boolean;
  feedbackText?: string;
};
