export type LoadingWrapperProps = {
  isLoading: boolean;
};
