import { FC } from 'react';

// ** Components
import { Spinner } from 'modules/core/components';

// ** Types
import type { LoadingWrapperProps } from './loading-wrapper.interfaces';

export const LoadingWrapper: FC<LoadingWrapperProps> = ({
  isLoading,
  children,
}) => {
  return (
    <>
      {isLoading && <Spinner />}
      {children}
    </>
  );
};
