// ** React Imports
import React from 'react';
import { NavLink } from 'react-router-dom';

// ** Third Party Components
import { X } from 'react-feather';

// ** Config
import themeConfig from 'modules/core/configs/theme-config';

// ** Styles
import styles from './menu-header.module.scss';

// ** Constants
import { protectedRoutes } from 'modules/core/constants/routes/protected-routes.constants';

// ** Types
import { MenuProps } from 'modules/core/layouts/menu/menu.interfaces';

export const MenuHeader: React.FC<MenuProps> = (props) => {
  // ** Props
  const { handleSetMenuVisibility } = props;

  return (
    <div className='navbar-header'>
      <ul className='nav navbar-nav flex-row align-items-center'>
        <li className='nav-item mr-auto w-75'>
          <NavLink to={protectedRoutes.dashboard} className='navbar-brand'>
            <span className='brand-logo w-100'>
              <img src={themeConfig.app.appLogoImage} alt='logo' />
            </span>
          </NavLink>
        </li>
        <li className='nav-item nav-toggle'>
          <div
            className={`nav-link modern-nav-toggle cursor-pointer ${styles.navLink}`}
          >
            <X
              onClick={() => handleSetMenuVisibility(false)}
              className={`toggle-icon icon-x d-block d-xl-none ${styles.icon}`}
              size={20}
            />
          </div>
        </li>
      </ul>
    </div>
  );
};
