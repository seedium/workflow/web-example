import { OnChangeValue } from 'react-select';

export interface IMultiOption {
  value: string;
  label: string;
  color: string;
  isFixed: boolean;
}

export interface IAction {
  action: string;
  removedValue: IMultiOption | undefined;
}

export interface IStylesState {
  data: { isFixed: boolean };
}

export type MultiSelectProps = {
  id: string;
  options: IMultiOption[];
  onChange: (newValue: OnChangeValue<IMultiOption, boolean>) => void;
  defaultValue?: IMultiOption[] | IMultiOption;
  isCreatable?: boolean;
  value?: IMultiOption;
  isClearable?: boolean;
  isLoading?: boolean;
  isDisabled?: boolean;
  closeMenuOnSelect?: boolean;
  name?: 'loading' | 'clear' | 'colors';
};
