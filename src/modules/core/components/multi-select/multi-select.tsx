import { FC } from 'react';
import Select from 'react-select';
import CreatableSelect from 'react-select/creatable';

// && Utils
import { selectThemeColors } from 'modules/core/utils/select-theme-colors';

// ** Types
import { MultiSelectProps } from './multi-select.interface';

export const MultiSelect: FC<MultiSelectProps> = ({
  options,
  isCreatable,
  value,
  onChange,
  name,
  ...props
}) => {
  return (
    <>
      {isCreatable ? (
        <CreatableSelect
          theme={selectThemeColors}
          options={options}
          isMulti
          value={value}
          onChange={onChange}
          name={name}
          className='react-select'
          classNamePrefix='select'
          {...props}
        />
      ) : (
        <Select
          theme={selectThemeColors}
          options={options}
          isMulti
          value={value}
          onChange={onChange}
          name={name}
          className='react-select'
          classNamePrefix='select'
          {...props}
        />
      )}
    </>
  );
};
