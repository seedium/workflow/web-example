import { FC } from 'react';
import { CustomInput } from 'reactstrap';

import type { RadioProps } from './radio.interface';

export const Radio: FC<RadioProps> = ({ color, ...props }) => {
  return (
    <CustomInput
      type='radio'
      className={color && `custom-control-${color}`}
      inline
      {...props}
    />
  );
};
