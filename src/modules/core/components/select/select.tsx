import { FC } from 'react';
import classnames from 'classnames';
import { FormGroup, Label, FormFeedback } from 'reactstrap';
import ReactSelect from 'react-select';

// ** Utils
import { selectThemeColors } from 'modules/core/utils/select-theme-colors';

// ** Types
import { SelectProps } from './select.interface';

export const Select: FC<SelectProps> = ({
  options,
  label,
  wrapperClassName,
  feedbackText = '',
  invalid = false,
  disabled,
  ...props
}) => {
  return (
    <FormGroup className={wrapperClassName}>
      {label && <Label>{label}</Label>}

      <ReactSelect
        theme={selectThemeColors}
        className={classnames('react-select', { 'is-invalid': invalid })}
        classNamePrefix='select'
        defaultValue={options[0]}
        options={options}
        isDisabled={disabled}
        isClearable={false}
        {...props}
      />
      {invalid && feedbackText && (
        <FormFeedback valid={!invalid}>{feedbackText}</FormFeedback>
      )}
    </FormGroup>
  );
};
