import { ReactNode } from 'react';

export type SidebarProps = {
  title: string;
  isOpen: boolean;
  onClose: () => void;
  closeBtn?: ReactNode;
  size?: 'lg' | 'sm';
  className?: string;
  bodyClassName?: string;
  contentClassName?: string;
  wrapperClassName?: string;
  headerClassName?: string;
  width?: number | string;
};
