import { FC } from 'react';
import { X } from 'react-feather';
import classnames from 'classnames';

// ** Types
import type { SidebarProps } from './sidebar.interface';

// ** Components
import { ModalBasic } from 'modules/core/components';

// ** Styles
import styles from './sidebar.module.scss';

export const Sidebar: FC<SidebarProps> = ({
  width = 358,
  isOpen,
  onClose,
  size,
  bodyClassName,
  contentClassName,
  wrapperClassName,
  className,
  title,
  children,
  closeBtn = <X className='cursor-pointer' size={20} onClick={onClose} />,
  ...props
}) => {
  return (
    <ModalBasic
      isOpen={isOpen}
      onClose={onClose}
      bodyProps={{
        className: classnames(
          'flex-grow-1 defaultSidePadding defaultBottomPadding',
          bodyClassName
        ),
      }}
      headerProps={{
        className:
          'defaultPadding pb-0 d-flex align-items-center bg-transparent',
        close: closeBtn,
        tag: 'div',
      }}
      header={
        <h4 className='modal-title'>
          <span className='align-middle'>{title}</span>
        </h4>
      }
      closeButtonText=''
      confirmButtonText=''
      contentClassName={classnames(styles.content, contentClassName)}
      modalClassName={classnames('modal-slide-in', wrapperClassName)}
      className={classnames({
        className,
        'sidebar-lg': size === 'lg',
        'sidebar-sm': size === 'sm',
      })}
      style={width ? { width: `${width}px` } : {}}
      {...props}
    >
      {children}
    </ModalBasic>
  );
};
