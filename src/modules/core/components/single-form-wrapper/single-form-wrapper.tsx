import React from 'react';
import { Link } from 'react-router-dom';
import { Card, CardBody } from 'reactstrap';

// ** Images
import { ReactComponent as Logo } from 'modules/core/assets/images/logo.svg';

// ** Styles
import '@core/scss/base/pages/page-auth.scss';
import styles from './single-form-wrapper.module.scss';

export const SingleFormWrapper: React.FC = ({ children }) => {
  return (
    <div className='auth-wrapper auth-v1 px-2'>
      <div className='auth-inner py-2'>
        <Card className='mb-0'>
          <CardBody>
            <Link
              className='brand-logo'
              to='/'
              onClick={(e) => e.preventDefault()}
            >
              <Logo className={styles.logo} />
            </Link>
            {children}
          </CardBody>
        </Card>
      </div>
    </div>
  );
};
