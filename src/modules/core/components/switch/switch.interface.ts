import { ChangeEvent } from 'react';

export type SwitchProps = {
  id: string;
  name: string;
  color?: 'secondary' | 'success' | 'danger' | 'warning' | 'info';
  defaultChecked?: boolean;
  disabled?: boolean;
  withIcons?: boolean;
  onChange: (e: ChangeEvent<HTMLInputElement>) => void;
  className?: string;
};
