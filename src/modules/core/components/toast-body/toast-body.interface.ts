import { ReactNode } from 'react';

import type { Color } from 'modules/core/types/theme.types';

export type ToastBodyProps = {
  text: string;
  icon?: ReactNode;
  color?: Color;
  textClassName?: string;
};
