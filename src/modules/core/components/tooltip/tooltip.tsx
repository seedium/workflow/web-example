import { FC } from 'react';
import classnames from 'classnames';
import { UncontrolledTooltip } from 'reactstrap';
import { HelpCircle } from 'react-feather';

// ** Types
import type { TooltipProps } from './tooltip.interfaces';

// ** Styles
import styles from './tooltip.module.scss';

export const Tooltip: FC<TooltipProps> = ({
  id,
  text,
  className,
  Icon = HelpCircle,
  iconSize = 24,
}) => {
  return (
    <>
      {Icon && <Icon size={iconSize} id={id} className={styles.icon} />}
      <UncontrolledTooltip
        target={id}
        placement='top'
        innerClassName={classnames(styles.tooltip, className)}
      >
        {text}
      </UncontrolledTooltip>
    </>
  );
};
