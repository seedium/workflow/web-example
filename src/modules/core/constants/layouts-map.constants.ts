import { FC } from 'react';

// ** Layouts
import { BlankLayout, MainLayout } from 'modules/core/layouts';

// ** Types
import { ILayout } from 'modules/core/types/layout.types';

export const layoutsMap: Record<ILayout, FC> = {
  BlankLayout,
  MainLayout,
};
