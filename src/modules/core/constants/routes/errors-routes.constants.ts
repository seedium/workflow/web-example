type ErrorsRoutesType = 'error' | 'accessDenied';

export const errorsRoutes: Record<ErrorsRoutesType, string> = {
  error: '/error',
  accessDenied: '/access-denied',
};
