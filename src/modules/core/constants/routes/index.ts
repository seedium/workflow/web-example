import { authRoutesMap } from './auth-routes.constants';
import { protectedRoutesMap } from './protected-routes.constants';
// types
import type { RouteType } from 'modules/core/types/routes.types';

const DefaultRoute: string = '/dashboard';

const Routes: RouteType[] = [...authRoutesMap, ...protectedRoutesMap];

export { DefaultRoute, Routes };
