import type { RouteType } from 'modules/core/types/routes.types';
import { Dashboard } from 'pages';

type ProtectedRoutesType = 'root' | 'dashboard';

export const protectedRoutes: Record<ProtectedRoutesType, string> = {
  root: '/admin',
  dashboard: '/admin/dashboard',
};

export const protectedRoutesMap: RouteType[] = [
  {
    path: protectedRoutes.dashboard,
    exact: true,
    component: Dashboard,
    layout: 'MainLayout',
  },
];
