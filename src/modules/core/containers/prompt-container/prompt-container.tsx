import React, { useEffect, useCallback, useState } from 'react';
import { useHistory, Prompt } from 'react-router-dom';
import { Location } from 'history';

// ** Hooks
import { useBoolean } from 'modules/core/hooks/use-boolean';

// ** Components
import { ModalBasic } from 'modules/core/components';

// ** Types
import type { PromptContainerProps } from './prompt-container.interfaces';

export const PromptContainer: React.FC<PromptContainerProps> = ({
  isDirtyForm,
  excludedRoute,
}) => {
  const [isOpenModal, setIsOpenModal] = useBoolean(false);
  const [nextRoutePath, setNextRoutePath] = useState<string>('');
  const [isConfirmedNavigation, setIsConfirmedNavigation] = useBoolean(false);

  const history = useHistory();

  const onBlock = useCallback(
    (newRoute: Location) => {
      if (excludedRoute && newRoute.pathname.includes(excludedRoute)) {
        return true;
      }

      if (!isConfirmedNavigation && isDirtyForm) {
        setIsOpenModal.on();
        setNextRoutePath(newRoute.pathname + newRoute.search);

        return false;
      }

      return true;
    },
    [isDirtyForm, isConfirmedNavigation, setIsOpenModal, excludedRoute]
  );

  const onConfirm = useCallback(() => {
    setIsConfirmedNavigation.on();
  }, [setIsConfirmedNavigation]);

  const onClose = useCallback(() => {
    setIsOpenModal.off();
  }, [setIsOpenModal]);

  useEffect(() => {
    if (isConfirmedNavigation) {
      history.push(nextRoutePath);
    }
  }, [nextRoutePath, isConfirmedNavigation, history]);

  return (
    <>
      <Prompt message={onBlock} when={isDirtyForm} />
      <ModalBasic
        header='Close without saving?'
        isOpen={isOpenModal}
        onClose={onClose}
        onConfirm={onConfirm}
      >
        You have unsaved changes. Are you sure you want to leave this page
        without saving?
      </ModalBasic>
    </>
  );
};
