import { FC, useContext } from 'react';
import { Redirect, Route, Switch } from 'react-router-dom';

// ** Context
import { AuthContext } from 'modules/auth/context/auth.context';

// Components
import { Admin, Auth, NotFound, AccessDenied } from 'pages';

// ** Constants
import { authRoutes } from 'modules/core/constants/routes/auth-routes.constants';
import { protectedRoutes } from 'modules/core/constants/routes/protected-routes.constants';
import { errorsRoutes } from 'modules/core/constants/routes/errors-routes.constants';

export const RouteRoot: FC = () => {
  const { isAuth } = useContext(AuthContext);

  return (
    <Switch>
      <Route
        path={authRoutes.authRoot}
        render={(props) =>
          !isAuth ? (
            <Auth {...props} />
          ) : (
            <Redirect to={protectedRoutes.dashboard} />
          )
        }
      />
      <Route
        path={protectedRoutes.root}
        render={(props) =>
          isAuth ? <Admin {...props} /> : <Redirect to={authRoutes.login} />
        }
      />
      <Route exact path={errorsRoutes.accessDenied} component={AccessDenied} />
      <Route exact path={errorsRoutes.error} component={NotFound} />
      <Redirect to={protectedRoutes.dashboard} />
    </Switch>
  );
};
