import { useCallback, useMemo } from 'react';
import { useLocation } from 'react-router-dom';

export const useUrlQuery = <T extends string>() => {
  const location = useLocation();
  const query = useMemo(() => new URLSearchParams(location.search), [location]);

  const get = useCallback(
    (queryKey: T) => {
      return query.get(queryKey);
    },
    [query]
  );

  return { get } as const;
};
