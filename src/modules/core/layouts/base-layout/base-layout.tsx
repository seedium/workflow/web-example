// ** React Imports
import { useState, useEffect, FC } from 'react';
import { useLocation } from 'react-router-dom';

// ** Store & Actions
import { setMenuCollapsed } from 'modules/core/redux/core.actions';

// ** Third Party Components
import classnames from 'classnames';
import { Navbar } from 'reactstrap';

// ** Custom Components
import { Footer, Sidebar, NavBar } from 'modules/core/layouts';

// ** Custom Hooks
import { useAppSelector, useAppDispatch } from 'modules/core/hooks/store-hooks';
import { useWindowSize } from 'modules/core/hooks/use-window-size';

// ** Constants
import { resolutionsMap } from 'modules/core/constants/resolutions.constants';

// ** Types
import { BaseLayoutProps } from './base-layout.interfaces';
import { NavbarType, FooterType } from 'modules/core/types/theme.types';

// ** Styles
import '@core/scss/base/core/menu/menu-types/vertical-menu.scss';
import '@core/scss/base/core/menu/menu-types/vertical-overlay-menu.scss';

export const BaseLayout: FC<BaseLayoutProps> = ({
  children,
  navbar,
  footer,
  menu,
}) => {
  // ** States
  const [isMounted, setIsMounted] = useState(false);
  const [menuVisibility, setMenuVisibility] = useState(false);
  const [width] = useWindowSize();

  // ** Store Vars
  const dispatch = useAppDispatch();
  const { menuCollapsed, menuHidden, navbarType, footerType } = useAppSelector(
    (state) => state.layout
  );

  // ** Vars
  const location = useLocation();

  // ** Toggles Menu Collapsed
  const handleMenuCollapsed = (val: boolean) => dispatch(setMenuCollapsed(val));

  const handleSetMenuVisibility = (visibility: boolean) => {
    setMenuVisibility(visibility);
  };

  //* * This function will detect the Route Change and will hide the menu on menu item click
  useEffect(() => {
    if (menuVisibility && width < resolutionsMap.xl) {
      setMenuVisibility(false);
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [location]);

  //* * ComponentDidMount
  useEffect(() => {
    setIsMounted(true);

    return () => setIsMounted(false);
  }, []);

  // ** Vars
  const footerClasses: Record<FooterType, string> = {
    static: 'footer-static',
    sticky: 'footer-fixed',
    hidden: 'footer-hidden',
  };

  const navbarWrapperClasses: Record<NavbarType, string> = {
    floating: 'navbar-floating',
    sticky: 'navbar-sticky',
    static: 'navbar-static',
    hidden: 'navbar-hidden',
  };

  const navbarClasses: Record<NavbarType, string> = {
    floating: 'floating-nav',
    sticky: 'fixed-top',
    static: 'navbar-static-top',
    hidden: 'd-none',
  };

  if (!isMounted) {
    return null;
  }

  return (
    <div
      className={classnames(
        `wrapper vertical-layout ${
          navbarWrapperClasses[navbarType] || 'navbar-floating'
        } ${footerClasses[footerType] || 'footer-static'}`,
        {
          // Modern Menu
          'vertical-menu-modern': width >= resolutionsMap.xl,
          'menu-collapsed': menuCollapsed && width >= resolutionsMap.xl,
          'menu-expanded': !menuCollapsed && width > resolutionsMap.xl,

          // Overlay Menu
          'vertical-overlay-menu': width < resolutionsMap.xl,
          'menu-hide': !menuVisibility && width < resolutionsMap.xl,
          'menu-open': menuVisibility && width < resolutionsMap.xl,
        }
      )}
      {...(menuHidden ? { 'data-col': '1-column' } : {})}
    >
      {!menuHidden ? (
        <Sidebar
          menu={menu || null}
          menuCollapsed={menuCollapsed}
          menuVisibility={menuVisibility}
          handleMenuCollapsed={handleMenuCollapsed}
          handleSetMenuVisibility={handleSetMenuVisibility}
        />
      ) : null}

      <Navbar
        expand='lg'
        light
        className={classnames(
          `header-navbar navbar align-items-center ${
            navbarClasses[navbarType] || 'floating-nav'
          } navbar-shadow`
        )}
      >
        <div className='navbar-container d-flex content'>
          {navbar ? (
            navbar({ handleSetMenuVisibility })
          ) : (
            <NavBar handleSetMenuVisibility={setMenuVisibility} />
          )}
        </div>
      </Navbar>
      {children}

      <div
        className={classnames('sidenav-overlay', {
          show: menuVisibility,
        })}
        onClick={() => setMenuVisibility(false)}
      />

      <footer
        className={classnames(
          `footer footer-light ${footerClasses[footerType] || 'footer-static'}`,
          {
            'd-none': footerType === 'hidden',
          }
        )}
      >
        {footer ? (
          footer({ footerType, footerClasses })
        ) : (
          <Footer footerType={footerType} footerClasses={footerClasses} />
        )}
      </footer>
    </div>
  );
};
