import { FooterType } from 'modules/core/types/theme.types';

export type FooterProps = {
  footerType: FooterType;
  footerClasses: Record<FooterType, string>;
};
