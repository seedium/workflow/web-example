// ** React Imports
import React from 'react';

// ** Components
import { AnimatedWrapper } from 'modules/core/components';

// ** Styles
import 'animate.css/animate.css';

export const LayoutWrapper: React.FC = ({ children }) => {
  return (
    <div className='app-content content overflow-hidden'>
      <div className='content-overlay' />
      <div className='header-navbar-shadow' />
      <AnimatedWrapper>{children}</AnimatedWrapper>
    </div>
  );
};
