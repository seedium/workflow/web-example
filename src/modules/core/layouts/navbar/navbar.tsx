// ** React Imports
import React from 'react';
import { NavItem, NavLink } from 'reactstrap';
import { Menu } from 'react-feather';

// ** Custom Components
import { Button } from 'modules/core/components';

// ** Services
import { auth0Service } from 'modules/auth/services/auth0.services';

// ** Types
import { NavbarProps } from './navbar.interfaces';

export const NavBar: React.FC<NavbarProps> = ({ handleSetMenuVisibility }) => {
  return (
    <>
      <div className='bookmark-wrapper d-flex align-items-center'>
        <ul className='navbar-nav d-xl-none'>
          <NavItem className='mobile-menu mr-auto'>
            <NavLink
              className='nav-menu-main menu-toggle hidden-xs is-active'
              onClick={() => handleSetMenuVisibility(true)}
            >
              <Menu className='ficon' />
            </NavLink>
          </NavItem>
        </ul>
      </div>
      <Button
        type='button'
        color='primary'
        className='ml-auto'
        onClick={() => auth0Service.logout({})}
      >
        Logout
      </Button>
    </>
  );
};
