// ** React Imports
import { Fragment, useState, useRef, FC, RefObject } from 'react';

// ** Third Party Components
import classnames from 'classnames';

// ** Types
import { SidebarProps } from './sidebar.interfaces';

export const Sidebar: FC<SidebarProps> = ({
  menuCollapsed,
  menu,
  ...restProps
}) => {
  // ** Menu Hover State
  const [menuHover, setMenuHover] = useState(false);

  // ** Ref
  const shadowRef: RefObject<HTMLDivElement> = useRef(null);

  // ** Function to handle Mouse Enter
  const onMouseEnter = () => {
    if (menuCollapsed) {
      setMenuHover(true);
    }
  };

  // ** Scroll Menu
  const scrollMenu = (container: HTMLElement) => {
    if (shadowRef && container.scrollTop > 0) {
      if (!shadowRef.current?.classList.contains('d-block')) {
        shadowRef.current?.classList.add('d-block');
      }
    } else if (shadowRef.current?.classList.contains('d-block')) {
      shadowRef.current?.classList.remove('d-block');
    }
  };

  return (
    <Fragment>
      <div
        className={classnames(
          'main-menu menu-fixed menu-accordion menu-shadow',
          'menu-light',
          {
            expanded: menuHover || menuCollapsed === false,
          }
        )}
        onMouseEnter={onMouseEnter}
        onMouseLeave={() => setMenuHover(false)}
      >
        {menu
          ? menu({
              ...restProps,
              menuHover,
              shadowRef,
              scrollMenu,
              menuCollapsed,
            })
          : null}
      </div>
    </Fragment>
  );
};
