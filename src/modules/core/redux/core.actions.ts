import { createAction } from 'typesafe-actions';

// ** Types
import { FooterType, NavbarType } from 'modules/core/types/theme.types';

enum CoreTypesNames {
  SET_CONTENT_WIDTH = '@core/SET_CONTENT_WIDTH',
  SET_MENU_COLLAPSED = '@core/SET_MENU_COLLAPSED',
  SET_MENU_HIDDEN = '@core/SET_MENU_HIDDEN',
  SET_SKIN = '@core/SET_SKIN',
  SET_NAVBAR_TYPE = '@core/SET_NAVBAR_TYPE',
  SET_FOOTER_TYPE = '@core/SET_FOOTER_TYPE',
}

export const setContentWidth = createAction(
  CoreTypesNames.SET_CONTENT_WIDTH
)<string>();

export const setMenuCollapsed = createAction(
  CoreTypesNames.SET_MENU_COLLAPSED
)<boolean>();

export const setMenuHidden = createAction(
  CoreTypesNames.SET_MENU_HIDDEN
)<boolean>();

export const setSkin = createAction(CoreTypesNames.SET_SKIN)<string>();

export const setNavbarType = createAction(
  CoreTypesNames.SET_NAVBAR_TYPE
)<NavbarType>();

export const setFooterType = createAction(
  CoreTypesNames.SET_FOOTER_TYPE
)<FooterType>();
