import { createReducer } from 'typesafe-actions';
import themeConfig from 'modules/core/configs/theme-config';

import type { CoreActions, CoreState } from 'modules/core/redux/core.types';
import {
  setContentWidth,
  setMenuCollapsed,
  setMenuHidden,
  setSkin,
  setNavbarType,
} from 'modules/core/redux/core.actions';

// ** Initial State
const coreDefaultState: CoreState = {
  skin: 'light',
  menuCollapsed: false,
  menuHidden: themeConfig.layout.menu.isHidden,
  contentWidth: themeConfig.layout.contentWidth,
  navbarType: 'floating',
  footerType: 'static',
};

export const coreReducer = createReducer<CoreState, CoreActions>(
  coreDefaultState
)
  .handleAction(setContentWidth, (state, { payload }) => ({
    ...state,
    contentWidth: payload,
  }))
  .handleAction(setMenuCollapsed, (state, { payload }) => ({
    ...state,
    menuCollapsed: payload,
  }))
  .handleAction(setMenuHidden, (state, { payload }) => ({
    ...state,
    menuHidden: payload,
  }))

  .handleAction(setSkin, (state, { payload }) => ({
    ...state,
    skin: payload,
  }))
  .handleAction(setNavbarType, (state, { payload }) => ({
    ...state,
    navbarType: payload,
  }));
