export type ApiConfigType = { headers: Record<string, string> };

interface SingleError {
  request_id?: string;
  message?: string;
  type?: string;
  code?: string;
}

interface ValidationError extends SingleError {
  errors?: { name?: string; keyword?: string; message?: string }[];
}

export type ResponseError = SingleError | ValidationError;

export const isValidationErrors = (
  errorObject: ResponseError
): errorObject is ValidationError => {
  if ('errors' in errorObject) {
    return true;
  }

  return false;
};

export enum ErrorsCodes {
  // eslint-disable-next-line no-magic-numbers
  UNAUTHORIZED = 401,
}
