export type HTMLInputSize = 'sm' | 'lg';

export type AddonType = 'prepend' | 'append';
