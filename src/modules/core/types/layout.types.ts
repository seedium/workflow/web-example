import { FC } from 'react';

export type ILayout = 'BlankLayout' | 'MainLayout';

export type LayoutType = {
  [key in ILayout]: FC;
};
