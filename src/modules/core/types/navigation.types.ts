import React from 'react';

export type NavigationType = {
  menuTitle: string;
  items: {
    title: string;
    icon: React.ReactNode;
    path: string;
    badge?: string;
    badgeText?: string;
    externalLink?: boolean;
    newTab?: boolean;
    isAvailable: boolean;
  }[];
};
