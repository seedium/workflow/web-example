import { AxiosRequestConfig } from 'axios';

export interface IRestApiService {
  get: <ResponseData>(
    endpoint: string,
    config?: AxiosRequestConfig
  ) => Promise<ResponseData>;
  post: <RequestData, ResponseData>(
    endpoint: string,
    data: RequestData
  ) => Promise<ResponseData>;
  put: <ResponseData>(
    endpoint: string,
    data: ResponseData
  ) => Promise<ResponseData>;
  delete: <ResponseData>(endpoint: string) => Promise<ResponseData>;
}
