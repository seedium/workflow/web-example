import React from 'react';
import { RouteComponentProps } from 'react-router-dom';
// types
import type { ILayout } from './layout.types';

export type RouteType = {
  className?: string;
  path: string;
  exact: boolean;
  component: React.LazyExoticComponent<React.FC<RouteComponentProps>>;
  layout: ILayout;
};
