import { ILayout } from 'modules/core/types/layout.types';
import { RouteType } from 'modules/core/types/routes.types';

export const getRoutesAndPaths = (layout: ILayout, routes: RouteType[]) => {
  const LayoutRoutes: RouteType[] = [];
  const LayoutPaths: string[] = [];

  routes.forEach((route) => {
    if (route.layout === layout) {
      LayoutRoutes.push(route);
      LayoutPaths.push(route.path);
    }
  });

  return { LayoutRoutes, LayoutPaths } as const;
};
