export const getValueFromSearchParams = <T extends object>(
  searchQuery: string,
  key: Extract<keyof T, string>
): string | null => {
  const searchParams = new URLSearchParams(searchQuery);

  const value = searchParams.get(key);

  return value || null;
};
