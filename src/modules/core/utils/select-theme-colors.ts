import { Theme } from 'react-select';

export const selectThemeColors = (theme: Theme) => ({
  ...theme,
  colors: {
    ...theme.colors,
    primary25: '#7367f01a',
    primary: '#7367f0',
    neutral10: '#7367f0',
    neutral20: '#d8d6de',
    neutral30: '#d8d6de',
  },
});
