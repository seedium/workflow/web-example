type LocalStorage = {
  isRememberAuthorization: boolean;
  expiresAt: number;
};

type LocalStorageKeys = keyof LocalStorage;

// ** Local Storage

export const addToLocalStorage = <
  T extends LocalStorageKeys,
  K extends LocalStorage[T]
>(
  key: T,
  data: K
): void => {
  localStorage.setItem(key, JSON.stringify(data));
};

export const getFromLocalStorage = <
  T extends LocalStorageKeys,
  K extends LocalStorage[T] | null
>(
  key: T
): K => {
  const item = localStorage.getItem(key);

  return item !== null ? JSON.parse(item) : null;
};

export const removeFromLocalStorage = <T extends LocalStorageKeys>(
  key: T
): void => {
  localStorage.removeItem(key);
};
