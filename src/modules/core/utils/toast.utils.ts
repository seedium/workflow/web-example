import { toast, ToastContent, ToastOptions } from 'react-toastify';

import type { ToastType } from '../types/toast.types';

export const showToast = (
  type: ToastType,
  content: ToastContent,
  options?: ToastOptions
) => {
  toast[type](content, {
    autoClose: 3000,
    hideProgressBar: true,
    closeButton: false,
    ...options,
  });
};
