import { FC, useEffect, useCallback } from 'react';
import { useHistory } from 'react-router-dom';
import { Auth0ParseHashError } from 'auth0-js';

// ** Components
import { Spinner } from 'modules/core/components';

// ** Constants
import { protectedRoutes } from 'modules/core/constants/routes/protected-routes.constants';

// ** Services
import { auth0Service } from 'modules/auth/services/auth0.services';

// ** Constants
import { authRoutes } from 'modules/core/constants/routes/auth-routes.constants';
import { AUTH0_ERRORS } from 'modules/auth/constants/auth0-errors.constants';

const Auth0Callback: FC = () => {
  const history = useHistory();

  const getAuth0ErrorDescription = (error: Auth0ParseHashError): string =>
    error.errorDescription || error.error_description || '';

  const validateErrors = useCallback(
    (err: Auth0ParseHashError) => {
      const errorDescription = getAuth0ErrorDescription(err);

      if (errorDescription.startsWith(AUTH0_ERRORS.EMAIL_NOT_VERIFIED)) {
        history.push(authRoutes.verifyEmail);
      } else {
        auth0Service.logout({});
      }
    },
    [history]
  );

  const handleParseUrlHash = useCallback(async () => {
    try {
      await auth0Service.parseUrlHash();
      history.push(protectedRoutes.dashboard);
    } catch (error) {
      const err = error as Auth0ParseHashError;

      validateErrors(err);
    }
  }, [validateErrors, history]);

  useEffect(() => {
    handleParseUrlHash();
  }, [handleParseUrlHash]);

  return <Spinner />;
};

export default Auth0Callback;
