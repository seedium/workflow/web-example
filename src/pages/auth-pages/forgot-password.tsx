import { FC, useState } from 'react';
import { Auth0Error } from 'auth0-js';
import { ChevronLeft } from 'react-feather';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { Link } from 'react-router-dom';
import { CardTitle, CardText, Form } from 'reactstrap';

// ** Components
import { Button, SingleFormWrapper, InputText } from 'modules/core/components';

// ** Services
import { auth0Service } from 'modules/auth/services/auth0.services';

// ** Types
import type { ForgotPasswordType } from 'modules/auth/types/forgot-password.types';

// ** Utils
import { showToast } from 'modules/core/utils/toast.utils';

// ** Hooks
import { useBoolean } from 'modules/core/hooks/use-boolean';

// ** Schemas
import { forgotPasswordSchema } from 'modules/auth/schemas/forgot-password.schemas';

// ** Constants
import { authRoutes } from 'modules/core/constants/routes/auth-routes.constants';

const ForgotPassword: FC = () => {
  const [isLoading, setIsLoading] = useBoolean(false);
  const [forgotSuccessMessage, setForgotSuccessMessage] = useState<string>('');

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm<ForgotPasswordType>({
    defaultValues: { email: '' },
    resolver: yupResolver(forgotPasswordSchema),
  });

  const onSubmit = async (data: ForgotPasswordType) => {
    setIsLoading.on();
    try {
      const message: string = await auth0Service.changePassword(data);

      setForgotSuccessMessage(message);
    } catch (error) {
      const err = error as Auth0Error;

      showToast('error', <>{err.description}</>);
    } finally {
      setIsLoading.off();
    }
  };

  return (
    <SingleFormWrapper>
      <CardTitle tag='h4' className='mb-1'>
        Forgot Password? 🔑
      </CardTitle>
      {forgotSuccessMessage ? (
        <CardText>{forgotSuccessMessage}</CardText>
      ) : (
        <>
          <CardText className='mb-2'>
            Enter your email and we'll send you instructions to reset your
            password.
          </CardText>
          <Form
            className='auth-forgot-password-form mt-2'
            onSubmit={handleSubmit(onSubmit)}
          >
            <InputText
              id='email'
              label='Email'
              placeholder='Enter your email'
              invalid={!!errors.email?.message}
              feedbackText={errors.email?.message}
              {...register('email')}
            />
            <Button color='primary' block type='submit' disabled={isLoading}>
              Send reset link
            </Button>
          </Form>
        </>
      )}

      <p className='text-center mt-2'>
        <Link to={authRoutes.login}>
          <ChevronLeft className='mr-25' size={14} />
          <span className='align-middle'>Back to login</span>
        </Link>
      </p>
    </SingleFormWrapper>
  );
};

export default ForgotPassword;
