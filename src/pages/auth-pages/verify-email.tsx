import React from 'react';
import { CardTitle, CardText } from 'reactstrap';

// ** Components
import { SingleFormWrapper } from 'modules/core/components';

const VerifyEmail: React.FC = () => {
  return (
    <SingleFormWrapper>
      <CardTitle tag='h4' className='mb-1'>
        Verify Email 🔑
      </CardTitle>
      <CardText>Please verify your email before proceed</CardText>
    </SingleFormWrapper>
  );
};

export default VerifyEmail;
