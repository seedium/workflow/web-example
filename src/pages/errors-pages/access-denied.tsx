import React from 'react';
import { Button } from 'reactstrap';
import { Link } from 'react-router-dom';
import notAuthImg from 'assets/images/pages/not-authorized.svg';

// ** Layouts
import { BlankLayout } from 'modules/core/layouts';

// ** Styles
import '@core/scss/base/pages/page-misc.scss';
import { protectedRoutes } from 'modules/core/constants/routes/protected-routes.constants';

const AccessDenied: React.FC = () => {
  return (
    <BlankLayout>
      <div className='misc-wrapper'>
        <div className='misc-inner p-2 p-sm-3'>
          <div className='w-100 text-center'>
            <h2 className='mb-3'>Access denied! 🔐</h2>
            <Button
              tag={Link}
              to={protectedRoutes.dashboard}
              color='primary'
              className='btn-sm-block mb-1'
            >
              Back to Dashboard
            </Button>
            <img
              className='img-fluid'
              src={notAuthImg}
              alt='Not authorized page'
            />
          </div>
        </div>
      </div>
    </BlankLayout>
  );
};

export default AccessDenied;
