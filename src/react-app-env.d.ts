// eslint-disable-next-line spaced-comment
/// <reference types="react-scripts" />

declare namespace NodeJS {
  interface ProcessEnv {
    NODE_ENV: 'development' | 'production' | 'test';
    REACT_APP_BASE_API_URL: string;
    REACT_APP_AUTH0_DOMAIN: string;
    REACT_APP_AUTH0_CLIENT_ID: string;
    REACT_APP_AUDIENCE: string;
    REACT_APP_AUDIENCE_MOBILE: string;
    REACT_APP_AUTH0_CONNECTION: string;
  }
}
